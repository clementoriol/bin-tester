'use strict';

const path = require('path');
const bin = require('../index');

// eslint-disable-next-line max-lines-per-function
describe('bin-tester', () => {
    const testNoPackage = './tests/test-cases';
    const testPackageNoBin = './tests/test-cases/no-bin';
    const testPackageObjectMultiple = './tests/test-cases/bin-object-multiple';
    const testPackageObjectSingle = './tests/test-cases/bin-object-single';
    const testPackageStringListArgs = './tests/test-cases/bin-string-list-args';
    const testPackageStringCwd = './tests/test-cases/bin-string-cwd';
    const testPackageStringEnv = './tests/test-cases/bin-string-env';
    const testPackageStringConsoleOutput = './tests/test-cases/bin-string/';
    const testPackageStringError = './tests/test-cases/bin-string-error';
    const testPackageNeverEnding = './tests/test-cases/bin-never-ending';

    it('should throw for package.json with no bin', async() => {
        expect.assertions(1);
        await expect(() => bin([], undefined, testPackageNoBin)).rejects.toThrow('\'bin\' property');
    });

    it('should throw if no package.json is found in the package directory', async() => {
        expect.assertions(1);
        await expect(() => bin([], undefined, testNoPackage)).rejects.toThrow('ENOENT');
    });

    it('should throw if provided binArguments that is not an array', async() => {
        expect.assertions(1);
        const binArguments = 0;
        await expect(() => bin(binArguments, undefined, testPackageStringConsoleOutput)).rejects.toThrow('binArguments');
    });

    it('should throw if the bin property of package.json is an object and a command is not specified', async() => {
        expect.assertions(1);
        await expect(() => bin([], undefined, testPackageObjectSingle)).rejects.toThrow('not a valid bin command');
    });

    it('should throw if the command cannot be found in the specified package.json bin property', async() => {
        expect.assertions(1);
        const command = 'foo';
        await expect(() => bin([], undefined, testPackageObjectSingle, {}, command)).rejects.toThrow('not a valid bin command');
    });

    it('should pass no arguments to bin if no binArguments provided', async() => {
        expect.assertions(1);
        const binArguments = undefined;
        // test case outputs arguments to stdout
        const expectedResults = { stdout: 'args: \n' };
        const results = await bin(binArguments, undefined, testPackageStringListArgs);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should pass arguments to bin if binArguments provided', async() => {
        expect.assertions(1);
        const binArguments = ['-c', 'my-config'];
        // test case outputs arguments to stdout
        const expectedResults = { stdout: `args: ${binArguments.join(', ')}\n` };
        const results = await bin(binArguments, undefined, testPackageStringListArgs);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should execute in the current directory if no workingDirectory provided', async() => {
        expect.assertions(1);
        const workingDirectory = undefined;
        // test case outputs workingDirectory to stdout
        const expectedResults = { stdout: `${process.cwd()}\n` };
        const results = await bin([], workingDirectory, testPackageStringCwd);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should execute in the specified directory if workingDirectory provided', async() => {
        expect.assertions(1);
        const workingDirectory = './tests/test-cases';
        // test case outputs workingDirectory to stdout
        const expectedResults = { stdout: `${path.resolve(process.cwd(), workingDirectory)}\n` };
        const results = await bin([], workingDirectory, testPackageStringCwd);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should pass additional environment variables to bin if environment provided', async() => {
        expect.assertions(1);
        const env = { FOO: 'bar' };
        // test case outputs environment variable FOO to stdout
        const expectedResults = { stdout: `env: FOO=${env.FOO}\n` };
        const results = await bin([], undefined, testPackageStringEnv, env);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should not pass additional environment variables to bin if no environment provided', async() => {
        expect.assertions(1);
        // test case outputs environment variable FOO to stdout
        const expectedResults = { stdout: 'env: FOO=\n' };
        const results = await bin([], undefined, testPackageStringEnv);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should look in the current directory for package.json if no packageDirectory provided', async() => {
        expect.assertions(1);
        // This package has no bin, so if run in current directory then throws a 'bin' property error
        await expect(() => bin([], undefined)).rejects.toThrow('\'bin\' property');
    });

    it('should look in the specified directory for package.json if packageDirectory provided', async() => {
        expect.assertions(1);
        const expectedResults = { code: 0, stdout: 'This is a bin with a string\n' };
        const results = await bin([], undefined, testPackageStringConsoleOutput);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should return stdout and stderr from the executed bin command', async() => {
        expect.assertions(1);
        // test case outputs to stdout and stderr
        const expectedResults = { stdout: 'This is a bin with a string\n', stderr: 'This is an error from a bin with a string\n' };
        const results = await bin([], undefined, testPackageStringConsoleOutput);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should return error code 0 if command executed successfully', async() => {
        expect.assertions(1);
        const expectedResults = { code: 0, error: null };
        const results = await bin([], undefined, testPackageStringConsoleOutput);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should return non-zero error code and error if command executed successfully', async() => {
        expect.assertions(2); // eslint-disable-line no-magic-numbers
        // test case throws an error
        const expectedResults = { code: 1, error: 'This is a test failure' };
        const results = await bin([], undefined, testPackageStringError);
        expect(results.code).toStrictEqual(expectedResults.code);
        expect(results.error.message).toMatch(expectedResults.error);
    });

    it('should successfully execute command for bin object with a single entry', async() => {
        expect.assertions(1);
        const expectedResults = { code: 0, stdout: 'This is a bin object with a single command\n' };
        const command = 'single';
        const results = await bin([], undefined, testPackageObjectSingle, {}, command);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should successfully execute command for bin object with multiple entries', async() => {
        expect.assertions(1);
        const expectedResults = { code: 0, stdout: 'This is command1 from a bin object with multiple commands\n' };
        const command = 'command1';
        const results = await bin([], undefined, testPackageObjectMultiple, {}, command);
        expect(results).toStrictEqual(expect.objectContaining(expectedResults));
    });

    it('should stop executing after some time if a timeout is specified', async() => {
        expect.assertions(4); // eslint-disable-line no-magic-numbers
        const command = 'never-ending';
        const timeoutDuration = 2500;
        const expectedResults = {
            code: 0,
            stdout:
              'This is a bin printing to console endlessly\nThis is a bin printing to console endlessly\nThis is a bin printing to console endlessly\n',
            stderr: '',
            error: 'Command failed' // The process timing out generates an error
        };
        const results = await bin(
            [],
            undefined,
            testPackageNeverEnding,
            {},
            command,
            timeoutDuration
        );

        expect(results.stdout).toStrictEqual(expectedResults.stdout);
        expect(results.code).toStrictEqual(expectedResults.code);
        expect(results.stderr).toStrictEqual(expectedResults.stderr);
        expect(results.error.message).toMatch(expectedResults.error);
    });
});
