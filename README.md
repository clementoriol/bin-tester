# bin-tester

Executes the package `bin` command in the specific folder with the specified arguments and environment variables and returns the results.

## Requirements

The following configuration is required:

- The `package.json` has a valid `bin` property with a string or object with CLI commands, which must be JavaScript files
- The node.js executable is available in the `PATH` (as `node`)

## Usage

The following is an example [jest](https://jestjs.io/) test showing usage.

```js
const bin = require('bin-tester');

it('should successfully execute and output JSON results to stdout if format specified as JSON', () => {
    // Specify output format to be JSON
    const testArgs = ['-f', 'json'];
    // Execute test with the specified arguments in the current folder
    const results = await bin(testArgs, './');

    expect(results.code).toStrictEqual(0);
    expect(results.stderr).toStrictEqual('');
    expect(results.stdout).toMatchSnapshot();
});
```

The `bin` command accepts the following arguments (all optional):

- `binArguments`: A string array of command line arguments and values to use when executing the package bin command (defaults to no arguments)
- `workingDirectory`: The working directory to execute the package bin command (defaults to `./`)
- `packageDirectory`: The directory with the package bin to be executed (defaults to `./`)
- `environment`: Environment variable key-value pairs (in addition to process.env)
- `command`:  The name of the bin command to be executed (if bin object versus string)
- `timeout`: A duration in ms after which the bin command will timeout and the promise will resolve

The command returns a Promise that resolves with an object with the following properties:

- `code`: exit code (0 if successful)
- `error`: error thrown during execution, if applicable
- `stderr`: output written to `stdout` during execution
- `stdout`: output written to `stderr` during execution

This Promise always resolves if the command was executed, even if an error was thrown during execution.  This Promise rejects if an error prevents the command from being executed.
