'use strict';

/**
 * Contains utilities for retrieving data from package.json files.
 *
 * @module packageUtils
 */

const fs = require('fs');
const path = require('path');

const fileName = 'package.json';

const getPackageJsonPath = (packageDirectory) => {
    return path.resolve(packageDirectory, fileName);
};

const getPackageJson = (packageJsonPath) => {
    return JSON.parse(fs.readFileSync(packageJsonPath));
};

/**
 * Returns the absolute path of the `bin` command from the package.json in the specified
 * directory (or current directory if none specified).
 *
 * @static
 * @param   {string} packageDirectory  The package directory (containing the package.json).
 * @param   {string} [command]         The name of the bin command to be executed (required if object).
 * @returns {string}                   The absolute path of the `bin` command from the
 *                                     package.json in the specified directory.
 * @throws  {TypeError}                The package.json does not contain a valid bin property
 *                                     (is undefined, or defined and not a string or object).
 * @throws  {Error}                    The specified bin command cannot be found in the
 *                                     package.json in the specified directory.
 */
const getBinPath = (packageDirectory, command) => {
    const packageJsonPath = getPackageJsonPath(packageDirectory);
    const pkg = getPackageJson(packageJsonPath);

    const binType = typeof(pkg.bin);
    if (!['string', 'object'].includes(binType)) {
        throw new TypeError(`${packageJsonPath} does not contain a valid 'bin' property`);
    }

    // eslint-disable-next-line default-case
    switch (binType) {
        case 'string':
            return path.resolve(packageDirectory, pkg.bin);
        case 'object':
            if (command && Object.keys(pkg.bin).includes(command)) {
                return path.resolve(packageDirectory, pkg.bin[command]);
            }
            throw new Error(`'${command}' is not a valid bin command name in ${packageJsonPath}`);
    }
};

module.exports.getBinPath = getBinPath;
