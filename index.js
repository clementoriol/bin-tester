'use strict';

/**
 * Executes the package bin command in the specific folder with the specified
 * arguments and returns the results.
 *
 * @module bin-tester
 */

const { exec } = require('child_process');
const { getBinPath } = require('./lib/packageUtils');

const validateBinArguments = (binArguments) => {
    if (typeof(binArguments) !== 'undefined' && !Array.isArray(binArguments)) {
        throw new TypeError('binArguments must be an array');
    }
};

/**
 * Execute bin as specified in package.json, with the specified arguments,
 * returning a promise that always resolves if the command was executed,
 * even if and Error was thrown during execution.  A promise rejection
 * indicates and error executing the command (e.g. invalid command name).
 *
 * @static
 * @param   {string[]} [binArguments]           An array of command line arguments and
 *                                              values to use when executing the package
 *                                              bin command.
 * @param   {string}   [workingDirectory=./]    The working directory to execute the package
 *                                              bin command.
 * @param   {string}   [packageDirectory=./]    The directory with the package bin to be executed.
 * @param   {object}   [environment={}]         Environment variable key-value pairs (in addition
 *                                              to process.env).
 * @param   {string}   [command]                The bin command to be executed.
 * @param   {number}   [timeout]                A timeout duration in ms.
 * @returns {Promise<object>}                   Promise resolving with an object with the exit
 *                                              code, error, stdout, and stderr from executing
 *                                              the package bin.
 */
const bin = (binArguments, workingDirectory = './', packageDirectory = './', environment = {}, command = '', timeout = 0) => {
    return new Promise(resolve => {
        validateBinArguments(binArguments);
        exec(`node ${getBinPath(packageDirectory, command)} ${binArguments && binArguments.length > 0 ? binArguments.join(' ') : ''}`,
            {
                cwd: workingDirectory,
                // The default env value is process.env, so need to add additional values.
                // Without this, PATH is not passed and the "node" command is not found.
                env: Object.assign({}, process.env, environment),
                timeout
            },
            (error, stdout, stderr) => resolve({
                code: error && error.code ? error.code : 0,
                error,
                stdout,
                stderr })
        );
    });
};

module.exports = bin;
