# Changelog

## Unreleased

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v1.2.0 (2020-11-28)

### Added

- Added support for package.json bin properties that are objects (in addition to strings) (#5)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#7) and GitLab Releaser (#8)

## v1.1.1 (2020-11-20)

### Fixed

- Fixed documentation errors for environment variables

## v1.1.0 (2020-11-20)

### Added

- Added support for providing environment variables (#6)

### Fixed

- Updated to latest dependencies

## v1.0.0 (2020-10-24)

### Added

- Added option to specify the package location (#1)

### Changed

- Renamed arguments for clarity and consistency

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Updated testing for more complete coverage (#4)

## v0.5.0 (2020-09-20)

Initial implementation
